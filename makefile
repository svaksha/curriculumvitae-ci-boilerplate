TEX = pandoc
FLAGS = --latex-engine=xelatex
BUILDDIR = public

src := $(wildcard *.yml)
TARGETS := $(patsubst %.yml,$(BUILDDIR)/%.pdf,$(src))

clsrc := $(filter-out README.md, $(wildcard *.md))
CLTARGETS := $(patsubst %.md,$(BUILDDIR)/%.pdf,$(clsrc))

all: $(TARGETS)

$(TARGETS): $(BUILDDIR)/%.pdf : %.yml $(TEMPLATE)
	$(eval TEMPLATE = $(shell egrep ^template $< | cut -d: -f2 | sed -e 's/#.*//'))
	$(TEX) $< -o $@ --template=$(TEMPLATE) $(FLAGS)

all: $(CLTARGETS)

$(CLTARGETS): $(BUILDDIR)/%.pdf : %.md $(CLTEMPLATE)
	$(eval CLTEMPLATE = $(shell egrep ^template $< | cut -d: -f2 | sed -e 's/#.*//'))
	$(TEX) $< -o $@ --template=$(CLTEMPLATE) $(FLAGS)

.PHONY: clean
clean:
	rm public/*.pdf
